Mathias










What types of values is Python using?
string and integer
victor: String integer float
ulrik: float, integer string
Mehrang: String, Number, Boolean, etc...
Nikandras: float, integer, string.
Bogdan: integer, float, string
Matas: integers, float and strings.
finn: integer, float, string.

How would you check a values type in a Python program?
use the interpreter by writing type()
victor: type()
ulrik: use type()
Mehrang: type()
Nikandras: >>>type(value)
Bogdan: type()
Matas: use Python type() function
finn: type()

What are variables?
A variable is a name that refers to a value
victor: values
ulrik: Reserved memory locations to store a value, like x=1
Mehrang: It’s a name that refers to a Value
Nik: Values rezerved in memory.
Bogdan:  Is a label for a memory location in which something is stored by the user.
Matas:Variables are containers for storing data values.
Finn: a variable is reference to an set value



What are reserved words?
reserved words are words that Python uses for other purposes. If one was to use these, python would get confused, so they just reserved it for ease of use.
Victor: defined commands not to be used any other way
Ulrik: Words as question explains, reserved by python. Not to be used as a function name or variable name or by another identifier.
Mehrang: AFAIK it’s 35 keywords which we can’t use and it’s predefined
Nik: commands that can only have one purpose.
Bogdan: Are the commands that python knows.
Matas: Reserved words (also called keywords) are defined with predefined meaning and syntax in the language. 
finn: reserved words are words which already are in use as python statements and therefore not can be use as something else
 





What is a statement?
a statement is a bit of code that the interpreter can execute, such as print
vic(^v^)
ulrik( ^_-)
Mehrang: It’s a section of code that represents a command or action
NIk: it’s a single command.
Bogdan: It’s a single command.
Matas: A statement in Python is a logical instruction which Python interpreter can read and execute.
Finn: a statement is an instruction 

What is the purpose of mnemonic naming?
Give at least 3 examples of mnemonic naming
            


What is the purpose of Mnemonic naming?:


Mathias: To make it easier to write, read and understand code. Instead of just having random garbled nonsense, it's better to have actual names for specific things, such as the hour/pay example in the book.
Victor: Monkey brain likes words and not “str”
Matas: the purpose of mnemonic naming is to make it easier to remember.
Ulrik: The meaning of mnemonic is to give meaning to names. So we can remember/access them more easily.
Nik: to make it easier to understand and write code.
Finn: Ensure that variables have unique and understandable names so troubleshoot gets a lot easier.
Bogdan:  Is an easy way to remember stuff that you have written in the program.
Give at least 3 examples of mnemonic naming
Matas:   DOES: Daddy only eats sandwiches.
              LOL: laugh out loud.
              FRIEND: Finn rushed in eating nine doughnuts. 
Bogdan:  R6: Rainbow 6 Siege 
	    GIT: Genius In Theory
	    WTF: Will  Turtle Fly
                FF: forfeit
Finn: ctemp = Celsius Temperature
	ftemp = Fahrenheit Temperature
	hrs = hours






Name pythons 6 operators and their syntax ?
Mathias.
Finn: *,**,+,-,/,=
Victor: () + - * / ** = ==
Ulrik: +-=%()
Matas: +-**/*=
Nik: +-**/*=
Bogdan: + - * ** / %
Mehrang:  +, -, *, /, **

What is the order of operations ?
Mathias.
victor: ** () > */ > +-+
Nik: **,(),*,/,-,+.
Matas:** () / * + -
Bogdan: ** * / ()  + -
Mehrang: (), **, *, /, +, - 

Finn: **,*,/,+,-,

What is concatenation ?
Victor: when strings become very good friends because operators apply social pressure
Mathias:or the social pressure is too much and they never meet again
Nik: but as time goes on you miss each other
Bogdan: Combining 2 string variables
Ulrik: adding strings to each other
Finn: combining 2 variables
How do you ask the user for input in a Python program ?
Mathias: prompt some input
you use the “input” command
Ulrik: input()
Matas: input command
Bogdan: input()
Finn: input()
How do you insert comments in a Python program ?
Victor: #hashtags
Mathias:#
Ulrik: #
Nik: #
Matas: #
Bogdan: #
Finn: #{comment}
